﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyCubeHealthBar : MonoBehaviour {
	
	public int maxHealth = 3;
	public int curHealth = 3;
	private float healthBarlength = Screen.width/2;
	private float healthBarlengthTemp = 50;
	private DateTime hitTimer = DateTime.MinValue;
	private ArrayList ids = new ArrayList();
	public Vector2 pos;
	//public Transform tempBar = gameObject.transform;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		AdjustcurHealth (0) ;
		
		if(hitTimer != DateTime.MinValue)
		{
			if (DateTime.Now.Millisecond % 500 > 250)
			{
				gameObject.renderer.enabled = false;
			}
			else
			{
				gameObject.renderer.enabled = true;
			}
			if (hitTimer < DateTime.Now)
			{
				hitTimer = DateTime.MinValue;
				gameObject.renderer.enabled = true;
				Physics2D.IgnoreLayerCollision(9,8, false);
				Physics2D.IgnoreLayerCollision(9,11, false);
			}
		}

		renderer.transform.localScale = new Vector3 (healthBarlength,transform.localScale.y, transform.localScale.z);
	}
	
	public void AdjustcurHealth (int adj) {
		curHealth += adj;
		if(curHealth < 0)
			curHealth = 0;
		if(curHealth > maxHealth)
			curHealth = maxHealth;
		if(maxHealth < 1)
			maxHealth = 1;
		healthBarlength = (healthBarlengthTemp)*(curHealth/(float)maxHealth);
		//tempBar.localScale.x = healthBarlength;
	}
	
	public void takeDamage(int dmg)
	{
		int[] temp = {dmg, int.MinValue};
		takeDamage (temp);
	}
	
	void takeDamage(int[] arg)
	{
		Debug.Log ("take damage");
		if(arg[1] == int.MinValue || !ids.Contains(arg[1]))
		{
			curHealth -= arg[0];
			if (curHealth <= 0) 
			{
				GameObject ghost = (GameObject)Instantiate(Resources.Load("Ghost"));
				ghost.SendMessage("setPosition", this.gameObject.transform.position);
				Destroy (this.gameObject);
			}
			if (arg[1] != int.MinValue)
			{
				ids.Add(arg[1]);
			}
		}
	}
}
