﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyMovement : MonoBehaviour {

	// Start and End Positions
	float startPos;
	float endPos;
	// Units the Enemy will be Moving to the Right
	public float unitsToMove = 6;
	// Enemy's Move Speed
	public int moveSpeed = 2;
	// Enemy Trigger Movement
	bool moveRight = true;
	DateTime launch;

	void Start()
	{
		// Initialize Enemy Tag
		gameObject.tag = "Enemy";
		launch = DateTime.Now;
		launch.AddSeconds (5);
	}
	void Awake() {
		// On Load Gets the Enemy's Starting X
		startPos = transform.position.x;
		// On Load Gets the Enemy's Starting End Position by Adding the Units to the Starting Position
		endPos = startPos + unitsToMove;
	}

	// Update is called once per frame
	void Update () {
		GameObject playerpos = GameObject.FindGameObjectsWithTag ("Player")[0];
		Vector3 dist = rigidbody2D.transform.position - playerpos.transform.position;
		if (dist.magnitude < 20) 
		{
			if (moveRight) {
				transform.position += Vector3.right * moveSpeed * Time.deltaTime;
			}
			else {
				transform.position += Vector3.left * moveSpeed * Time.deltaTime;
			}
			if (launch < DateTime.Now) 
			{
				GameObject proj = (GameObject)Instantiate(Resources.Load("GrapeProjectile"));
				// Calls methods from the spawned GameObject proj
				proj.SendMessage("setVector", Vector3.right);
				proj.SendMessage("setPosition", gameObject.rigidbody2D.transform.position);
				proj = (GameObject)Instantiate(Resources.Load("GrapeProjectile"));
				// Calls methods from the spawned GameObject proj
				proj.SendMessage("setVector", Vector3.left);
				proj.SendMessage("setPosition", gameObject.rigidbody2D.transform.position);
				proj = (GameObject)Instantiate(Resources.Load("GrapeProjectile"));
				// Calls methods from the spawned GameObject proj
				proj.SendMessage("setVector", Vector3.up);
				proj.SendMessage("setPosition", gameObject.rigidbody2D.transform.position);
				proj = (GameObject)Instantiate(Resources.Load("GrapeProjectile"));
				// Calls methods from the spawned GameObject proj
				proj.SendMessage("setVector", Vector3.down);
				proj.SendMessage("setPosition", gameObject.rigidbody2D.transform.position);
				proj = (GameObject)Instantiate(Resources.Load("GrapeProjectile"));
				// Calls methods from the spawned GameObject proj
				proj.SendMessage("setVector", new Vector3(1,1,0).normalized);
				proj.SendMessage("setPosition", gameObject.rigidbody2D.transform.position);
				proj = (GameObject)Instantiate(Resources.Load("GrapeProjectile"));
				// Calls methods from the spawned GameObject proj
				proj.SendMessage("setVector", new Vector3(-1,1,0).normalized);
				proj.SendMessage("setPosition", gameObject.rigidbody2D.transform.position);
				proj = (GameObject)Instantiate(Resources.Load("GrapeProjectile"));
				// Calls methods from the spawned GameObject proj
				proj.SendMessage("setVector", new Vector3(1,-1,0).normalized);
				proj.SendMessage("setPosition", gameObject.rigidbody2D.transform.position);
				proj = (GameObject)Instantiate(Resources.Load("GrapeProjectile"));
				// Calls methods from the spawned GameObject proj
				proj.SendMessage("setVector", new Vector3(-1,-1,0).normalized);
				proj.SendMessage("setPosition", gameObject.rigidbody2D.transform.position);
				launch = DateTime.Now.AddSeconds(UnityEngine.Random.Range(2,5));
			}
			if(transform.position.x > endPos) {
				moveRight = false;
			}
			if(transform.position.x < startPos - endPos) {
				moveRight = true;
			}
		}
	}
}
