﻿using UnityEngine;
using System.Collections;
using System;

public class GhostMovement : MonoBehaviour {

	private DateTime end = DateTime.MaxValue;
	public int movementspeed = 3;

	// Use this for initialization
	void Start () {
		end = DateTime.Now.AddSeconds (1.5);
	}
	
	// Update is called once per frame
	void Update () {
		if(end < DateTime.Now)
		{
			Destroy(this.gameObject);
		}
		else
		{
			this.gameObject.transform.position += Vector3.up * movementspeed * Time.deltaTime;
		}
	}

	void setPosition(Vector3 pos)
	{
		//end = DateTime.Now.AddSeconds (1.5);
		gameObject.transform.position = pos;
	}
}
