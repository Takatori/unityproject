﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyHealth : MonoBehaviour {

	public int health = 15;
	private ArrayList ids = new ArrayList();
	DateTime blink = DateTime.MinValue;
	public Boolean tablet = false;
	private Boolean tabletBroken = false;


	void Update()
	{
		if(DateTime.Now < blink)
		{
			if (DateTime.Now.Millisecond % 300 > 150)
			{
				gameObject.renderer.enabled = false;
			}
			else
			{
				gameObject.renderer.enabled = true;
			}
		}
		else if(blink != DateTime.MinValue)
		{
			blink = DateTime.MinValue;
			gameObject.renderer.enabled = true;
		}
	}

	void takeDamage(int dmg)
	{
		int[] temp = {dmg, int.MinValue};
		takeDamage (temp);
	}

	void takeDamage(int[] arg)
	{
		if(arg[1] == int.MinValue || !ids.Contains(arg[1]))
		{
			blink = DateTime.Now.AddSeconds(.5); 
			health -= arg[0];
			if (health <= 0) 
			{
				if (!tablet)
				{
					GameObject ghost = (GameObject)Instantiate(Resources.Load("Ghost"));
					ghost.SendMessage("setPosition", this.gameObject.transform.position);
					int exp = PlayerPrefs.GetInt("Exp");
					int level = PlayerPrefs.GetInt("Level");
					exp++;
					if(exp > level*level + 5*level)
					{
						PlayerPrefs.SetInt("Level", level+1);
						PlayerPrefs.SetInt("Exp",0);
					}
					else
					{
						PlayerPrefs.SetInt ("Exp", exp);
					}
					Destroy (this.gameObject);
				}
				else
				{
					tabletBroken = true;
				}
			}
			if (arg[1] != int.MinValue)
			{
				ids.Add(arg[1]);
			}
		}
	}

	public void OnGUI () {
		if (tabletBroken) {
			Time.timeScale = 0;
			
			float boxWidth = Screen.width*55/100;
			float boxHeight = Screen.height*55/100;
			float numButtons = 7;
			
			// Make a background box
			GUI.Box(new Rect((Screen.width/2)-(boxWidth/2),(Screen.height/2)-(boxHeight/2),boxWidth,boxHeight), "Congratulations! You won the level!");
			
			// Make the LoadLevel1 button. If it is pressed, Application.Loadlevel (5) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(1*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Load Level 1")) {
				Application.LoadLevel(5);
			}
			
			// Make the LoadLevel2 button. If it is pressed, Application.Loadlevel (6) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(2*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Load Level 2")) {
				Application.LoadLevel(6);
			}
			
			// Make the LoadLevel3 button. If it is pressed, Application.Loadlevel (7) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(3*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Load Level 3")) {
				Application.LoadLevel(7);
			}
			
			// Make the SurvivalMode button. If it is pressed, Application.Loadlevel (8) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(4*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Survival Mode")) {
				Application.LoadLevel(8);
			}
			
			// Make the Instructions button. If it is pressed, Application.Loadlevel (2) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(5*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Instructions")) {
				Application.LoadLevel(2);
			}
			
			// Make the Objective button. If it is pressed, Application.Loadlevel (1) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(6*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Objective")) {
				Application.LoadLevel(1);
			}
			
			// Make the Quit button.
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(7*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Quit")) {
				Application.Quit();
				Destroy (this.gameObject);
			}

			/*
			Time.timeScale = 0;

			float boxWidth = Screen.width / 2;
			float boxHeight = Screen.height / 2;
			float numButtons = 4;
		
			// Make a background box
			GUI.Box (new Rect ((Screen.width / 2) - (boxWidth / 2), (Screen.height / 2) - (boxHeight / 2), boxWidth, boxHeight), "Congratulations! You completed the level!");
		
			// Make the Main Menu button. If it is pressed, Application.Loadlevel (0) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(1*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Main Menu")) {
				Application.LoadLevel(0);
			}

			// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (2 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Load Level 1")) {
				Application.LoadLevel (1);
			}
		
			// Make the second button. If it is pressed, Application.Loadlevel (2) will be executed
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (3 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Load Level 2")) {
				Application.LoadLevel (2);
			}
		
			// Make the third button. If it is pressed, Application.Loadlevel (3) will be executed
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (4 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Load Level 3")) {
				Application.LoadLevel (3);
			}
		
			/*
			// Make the quit button.
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (5 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Quit Level")) {
				Application.Quit ();
				Application.LoadLevel(0);
			}
			*/
		}
	}
}
