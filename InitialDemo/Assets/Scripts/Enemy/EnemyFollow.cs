﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyFollow : MonoBehaviour {
	
	// Start and End Positions
	float startPos;
	// how close you have to be in order to have them follow
	public int followDistance = 17;
	// Units the Enemy will be Moving to the Right
	public float unitsToMove = 6;
	// Enemy's Move Speed
	public int moveSpeed = 2;
	// Enemy Trigger Movement
	bool moveRight = true;
	DateTime launch;
	Vector3 dir = new Vector3();
	DateTime randomness = DateTime.MinValue;
	String tagToHit = "Player";

	void Start()
	{
		// Initialize Enemy Tag
		gameObject.tag = "Enemy";
		launch = DateTime.Now;
		launch.AddSeconds (5);
	}
	void Awake() {
		// On Load Gets the Enemy's Starting X
		startPos = transform.position.x;
	}

	// Event collision
	void OnCollisionEnter2D(Collision2D c)
	{
		// When hit (collision with an Enemy/Player),
		// send message to decrement the Enemy/Player's health
		if (String.Equals(c.gameObject.tag, tagToHit, StringComparison.OrdinalIgnoreCase))
		{
			c.gameObject.SendMessage("takeDamage", 10);
		}		
	}
	
	// Update is called once per frame
	void Update () {
		GameObject playerpos = GameObject.FindGameObjectsWithTag ("Player")[0];
		Vector3 dist = rigidbody2D.transform.position - playerpos.transform.position;
		if (dist.magnitude < 17) 
		{
			Vector3 plot = new Vector3(playerpos.transform.position.x - rigidbody2D.transform.position.x,
			                           playerpos.transform.position.y - rigidbody2D.transform.position.y,
			                           0).normalized;
			dir = (plot * moveSpeed * Time.deltaTime + dir.normalized *10*moveSpeed * Time.deltaTime) / 11;
		}

		if( DateTime.Now > randomness)
		{
			randomness = DateTime.Now.AddSeconds(UnityEngine.Random.Range(2,5)*.1);
			dir = new Vector3();
			dir.x = UnityEngine.Random.Range(-4,4);
			dir.y = UnityEngine.Random.Range(-4,4);
			dir = dir.normalized * moveSpeed * Time.deltaTime;
		}

		transform.position += dir;
	}
}
