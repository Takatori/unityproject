﻿using UnityEngine;
using System.Collections;
using System;

public class SurvivalSpawn : MonoBehaviour {

	ArrayList spawns = new ArrayList();
	int wave = 0;
	DateTime updateWave = DateTime.Now;
	DateTime spawnTimer = DateTime.MaxValue;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		if(DateTime.Now > updateWave)
		{
			if (spawns.Count == 0) 
			{
				if(DateTime.Now > spawnTimer)
				{
					GameObject spawn;
					wave++;
					if(wave == 11)
					{
						PlayerPrefs.SetInt("GhostGoat",1);
					}
					for(int i = 0; i < wave*2; i++)
					{
						spawn = (GameObject)Instantiate(Resources.Load("Carrot"));
						if (i%15 == 14 || i % 15 == 13)
						{
							spawn = (GameObject)Instantiate(Resources.Load("Grape"));
						}
						if(i%2 == 0)
						{
							spawn.transform.position = this.transform.position;
						}
						else
						{
							spawn.transform.position = this.transform.position + 10*Vector3.up + 20*Vector3.right;
						}
						spawns.Add(spawn);
					}
					spawnTimer = DateTime.MaxValue;
				}
				else
				{
					if(spawnTimer == DateTime.MaxValue)
					{
						spawnTimer = DateTime.Now.AddSeconds(6);
					}
				}
			}
			else
			{
				foreach(GameObject spawn in spawns)
				{
					if(spawn == null)
					{
						spawns.Remove(spawn);
					}
				}
			}
			updateWave = DateTime.Now.AddSeconds(3);
	    }
	}

	void OnGUI()
	{	
		String text = "wave " + wave;
		if(spawnTimer != DateTime.MaxValue)
		{
			text = "Spawn in " + (spawnTimer - DateTime.Now).Seconds;
		}
		GUI.Box (new Rect (150, 60, 75, 25), text);
	}
}
