﻿using UnityEngine;
using System.Collections;

public class CutsceneTimer : MonoBehaviour {

	public float timeLeft = 5;

	public Color color1 = Color.red;
	public Color color2 = Color.blue;
	public float duration = 3.0F;

	void ClearFlags() {
		camera.clearFlags = CameraClearFlags.SolidColor;
	}
	
	// Use this for initialization
	void Start () {
		/*
		camera.backgroundColor.r = 215;
		camera.backgroundColor.g = 186;
		camera.backgroundColor.b = 145;
		camera.backgroundColor.a = 5;
		*/
	}
	
	// Update is called once per frame
	void Update () {
		float t = Mathf.PingPong(Time.time, duration) / duration;
		camera.backgroundColor = Color.Lerp(color1, color2, t);
		
		timeLeft -= Time.deltaTime;
		if ( timeLeft < 0 )
		{
			if (Application.loadedLevel == 0)
				Application.LoadLevel(4);
		}

		if(Input.GetMouseButtonDown(0)){
			if (Application.loadedLevel == 1)
				Application.LoadLevel(4);
			if (Application.loadedLevel == 2)
				Application.LoadLevel(4);
		}
	}



	public void OnGUI () {
		if (timeLeft < 0 && Application.loadedLevel == 3) {
		
			float boxWidth = Screen.width / 2;
			float boxHeight = Screen.height / 2;
			float numButtons = 4;
		
			// Make a background box
			GUI.Box (new Rect ((Screen.width / 2) - (boxWidth / 2), (Screen.height / 2) - (boxHeight / 2), boxWidth, boxHeight), "Main Menu");
		
			// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (1 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Load Level 1")) {
				Application.LoadLevel (1);
			}
		
			// Make the first button. If it is pressed, Application.Loadlevel (0) will be executed
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (2 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Load Level 2")) {
				Application.LoadLevel (2);
			}
		
			// Make the first button. If it is pressed, Application.Loadlevel (0) will be executed
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (3 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Load Level 3")) {
				Application.LoadLevel (3);
			}
		
			// Make the second button.
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (4 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Quit")) {
				Application.Quit ();
				Destroy (this.gameObject);
			}
		}
	}
}
