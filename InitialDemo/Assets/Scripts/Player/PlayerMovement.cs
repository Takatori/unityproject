﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerMovement : MonoBehaviour {

	// Player's starting speed
	public int playerspeed = 5;
	// Player's staring health
	private int health = 100;
	private int level;
	private int exp;

	private DateTime blink;
	public Vector3 dir;
	private int key = 0;

	void Start()
	{
		Time.timeScale = 1;

		// Initialize Player object
		gameObject.tag = "Player";
		Animator ani = transform.root.GetComponent<Animator> ();
		ani.speed = 1.4f;

		try
		{
			level = PlayerPrefs.GetInt("Level");
			exp = PlayerPrefs.GetInt ("Exp");
			Debug.Log("Level = " + level);
		}
		catch(Exception e)
		{
			PlayerPrefs.SetInt("Level", 1);
			PlayerPrefs.SetInt("Exp", 0);
			Debug.Log("caught exception ");
		}

	}
	
	// Update is called once per frame
	void Update () {
		float hor = Input.GetAxis ("Horizontal");
		float vert = Input.GetAxis ("Vertical");

		dir = new Vector3 ();

		if(vert < 0)
		{
			dir += Vector3.down * Mathf.Abs(vert);
		}
		if(vert > 0)
		{
			dir += Vector3.up * Mathf.Abs(vert);
		}
		if(hor < 0)
		{
			dir += Vector3.left * Mathf.Abs(hor);
		}
		if(hor > 0)
		{
			dir += Vector3.right * Mathf.Abs(hor);
		}
		rigidbody2D.transform.position += dir.normalized * playerspeed * Time.deltaTime; 

	}

	// Event collision
	void OnCollisionEnter2D(Collision2D c)
	{
		// When hit (collision with an Enemy/Player),
		// send message to decrement the Enemy/Player's health
		if (String.Equals(c.gameObject.tag, "Key", StringComparison.OrdinalIgnoreCase))
		{
			Destroy(c.gameObject);
			key = 1;
		}		
		else if (String.Equals(c.gameObject.tag, "Gate", StringComparison.OrdinalIgnoreCase) && key == 1)
		{
			Destroy (c.gameObject);
		}
	}
}
