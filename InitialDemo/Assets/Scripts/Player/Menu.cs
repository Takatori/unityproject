﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	
	void OnGUI () {
		float numButton = 6;

		// Make a background box
		GUI.Box(new Rect(10,10,120,30*(numButton+1)), "Menu");

		// Make the main menu button.
		if (GUI.Button (new Rect (20,30*(numButton-5)+10,100,20), "Main Menu")) {
			Application.LoadLevel (4);
		}

		// Make the LoadLevel1 button. If it is pressed, Application.Loadlevel (5) will be executed
		if(GUI.Button(new Rect(20,30*(numButton-4)+10,100,20), "Load Level 1")) {
			Application.LoadLevel(5);
		}

		// Make the LoadLevel2 button. If it is pressed, Application.Loadlevel (6) will be executed
		if(GUI.Button(new Rect(20,30*(numButton-3)+10,100,20), "Load Level 2")) {
			Application.LoadLevel(6);
		}

		// Make the LoadLevel3 button.  If it is pressed, Application.Loadlevel (7) will be executed
		if (GUI.Button (new Rect (20,30*(numButton-2)+10,100,20), "Load Level 3")) {
			Application.LoadLevel (7);
		}

		// Make the SurvivalMode button.  If it is pressed, Application.Loadlevel (8) will be executed
		if (GUI.Button (new Rect (20,30*(numButton-1)+10,100,20), "Survival Mode")) {
			Application.LoadLevel (8);
		}
		
		// Make the quit button.  If it is pressed, Application will be quit
		if(GUI.Button(new Rect(20,30*(numButton-0)+10,100,20), "Quit")) {
			Application.Quit();
			Destroy(this.gameObject);
			/*
			GUI.Box(new Rect(Screen.width/2,Screen.height/2,100,90), "Are you sure you want to quit?");
			if(GUI.Button(new Rect(Screen.width/2+10,Screen.height/2+30,80,20), "Yes, I quit.")) {
				Application.Quit();
			}
			if(GUI.Button(new Rect(Screen.width/2+10,Screen.height/2+60,80,20), "No, I still want to play!")) {
				Application.CancelQuit();
			}
			*/
		}
	}
}