﻿using UnityEngine;
using System.Collections;
using System;

public class ClickExample : MonoBehaviour {

	DateTime next = DateTime.Now;
	private Animator animator;
	public int facing = 0;
	private int tomatoes = 5;
	private int idnum = 0;
	public Vector2 pos;
	int level = 1;
	int untilNext = 1;
	DateTime updateGUI = DateTime.Now;
	int ghostGoat = 0;
	int goatSpawn = 0;
	GameObject goat;
	DateTime goatTimer = DateTime.MinValue;
	public Texture2D tomatoTexture;

	void Start()
	{
		int i = 10;
		try
		{
			i = PlayerPrefs.GetInt("Level");
			Debug.Log("Damage = " + i);
		}
		catch(Exception e)
		{
			Debug.Log("caught exception ");
		}

		PlayerPrefs.SetInt("Damage", 5 + i*2);
		animator = this.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () 
	{
		try
		{
			ghostGoat = PlayerPrefs.GetInt("GhostGoat");
		}
		catch(Exception e)
		{
			ghostGoat = 0;
		}

		// Pixel Position of the mouse
		Vector3 mouseVec = Input.mousePosition;
		// Pixel position of the thing that this is attached to.
		Vector2 objVec = Camera.allCameras[0].WorldToScreenPoint(gameObject.transform.position);
		//float total = Math.Abs(mouseVec.x - objVec.x) + Math.Abs(mouseVec.y - objVec.y);
		Vector3 vec = new Vector3((mouseVec.x - objVec.x), (mouseVec.y - objVec.y),0);
		if(vec.y < 0 && Math.Abs(vec.y) > Math.Abs(vec.x))
		{
			animator.SetInteger("Direction", 1);
			facing = 1;
		}
		if(vec.y > 0 && Math.Abs(vec.y) > Math.Abs(vec.x))
		{
			animator.SetInteger("Direction", 0);
			facing = 0;
		}
		if(vec.x < 0 && Math.Abs(vec.x) > Math.Abs(vec.y))
		{
			animator.SetInteger("Direction", 3);
			facing = 3;
		}
		if(vec.x > 0 && Math.Abs(vec.x) > Math.Abs(vec.y))
		{
			animator.SetInteger("Direction", 2);
			facing = 2;
		}

		// If the left mouse button is pressed.
		if (Input.GetMouseButtonDown(1) && next < DateTime.Now && tomatoes > 0)
		{
			GameObject proj = (GameObject)Instantiate(Resources.Load("Projectile"));
			// Calls methods from the spawned GameObject proj
			proj.SendMessage("setVector", vec.normalized);
			proj.SendMessage("setDestination", Camera.allCameras[0].ScreenToWorldPoint(mouseVec+new Vector3(0,0,10)));
			proj.SendMessage("setPosition", gameObject.rigidbody2D.transform.position);
			//Debug.Log("game object " + objVec.x + " " + objVec.y + " This is the mousePosition = " + Input.mousePosition);
			next = DateTime.Now.AddSeconds(.75);
			tomatoes--;
		}

		if(Input.GetMouseButton(0) && next < DateTime.Now)
		{
			GameObject gas = (GameObject)Instantiate(Resources.Load("Gas"));
			// Calls methods from the spawned GameObject proj
			Vector3 pos = gameObject.rigidbody2D.transform.position;
			Vector3 dir = new Vector3(); 
			if(facing == 0)
			{
				dir += Vector3.up;
				gas.renderer.sortingOrder=-1;
			}
			if(facing == 1)
			{
				dir += Vector3.down;
			}
			if(facing == 2)
			{
				dir += Vector3.right;
			}
			if(facing == 3)
			{
				dir += Vector3.left;
			}
			pos += dir/3f;

			gas.SendMessage("setPosition", pos);
			gas.SendMessage("setVector", vec.normalized);
			gas.SendMessage("setId", idnum++);
			Debug.Log("Send postition " + pos);
			next = DateTime.Now.AddSeconds(.3);
		}

		if(Input.GetKeyDown("e") && ghostGoat == 1 && goatTimer == DateTime.MaxValue) 
		{
			goat = (GameObject)Instantiate(Resources.Load("GhostGoat"));
			goatSpawn = 1;

			// Need to enable and disable the collider due to 
			//bug that doesnt let the layers ignore collisions sometimes
			gameObject.collider2D.enabled = false;
			Physics2D.IgnoreLayerCollision(9,8);
			Physics2D.IgnoreLayerCollision(9,11);
			gameObject.collider2D.enabled = true;
			gameObject.renderer.enabled = false;
			goatTimer = DateTime.Now.AddSeconds(5);
		}

		if(DateTime.Now > goatTimer)
		{
			if(goatSpawn == 1)
			{
				goatSpawn = 0;
				Destroy(goat);
				goatTimer = DateTime.Now.AddSeconds(30);
				gameObject.renderer.enabled = true;
				Physics2D.IgnoreLayerCollision(9,8, false);
				Physics2D.IgnoreLayerCollision(9,11, false);
			}
			else
			{
				goatTimer = DateTime.MaxValue;
			}
		}
	}

	// checks for collisions with tomatoes
	void OnCollisionEnter2D(Collision2D c)
	{
		if (String.Equals(c.gameObject.tag, "Tomatoes", StringComparison.OrdinalIgnoreCase))
		{
			tomatoes += 5;
			Destroy(c.gameObject);
		}	
	}

	void OnGUI () {
		
		GUI.Box (new Rect (10, 230, 75, 75), tomatoes + " Tomatoes\n Left");
		GUI.DrawTexture (new Rect(10,250,75,55), tomatoTexture);
		GUI.Box (new Rect (10, 330, 75, 40), "Level " + level 
		         + "\n level in " + untilNext);
		if( ghostGoat == 1)
		{
			String text = "Ghost Goat Avaliable";
			if(goatSpawn == 0 && goatTimer != DateTime.MaxValue)
			{
				text = text + " in " + (goatTimer - DateTime.Now).Seconds;
			}
			else if( goatSpawn == 1)
			{
				text = "Over in " + (goatTimer-DateTime.Now).Seconds;
			}
			GUI.Box (new Rect (10, 395, 175, 40), text +  "\nPress E to Activate");
		}
		if(DateTime.Now > updateGUI)
		{
			level = PlayerPrefs.GetInt ("Level");
			untilNext = (level*level + 5*level + 1) - PlayerPrefs.GetInt ("Exp");
			updateGUI = DateTime.Now.AddSeconds(.5);
		}
	}
}
