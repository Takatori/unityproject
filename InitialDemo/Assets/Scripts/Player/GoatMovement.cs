﻿using UnityEngine;
using System.Collections;
using System;

public class GoatMovement : MonoBehaviour {

	private int level = 1;

    // Use this for initialization
	void Start () {
	    // Initialize projectile tag
		gameObject.tag = "Projectile";
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 temp = GameObject.FindGameObjectsWithTag ("Player")[0].rigidbody2D.transform.position;
		this.transform.position = temp;
	}

	// Event collision
	void OnCollisionEnter2D(Collision2D c)
	{
		// When hit (collision with an Enemy/Player),
		// send message to decrement the Enemy/Player's health
		if (String.Equals(c.gameObject.tag, "Enemy", StringComparison.OrdinalIgnoreCase))
		{
			int dmg = 2 * level + 3;
			c.gameObject.SendMessage("takeDamage", dmg);
			// This is only implemented in the beta version of unity
			// This would make it so that our gas would collide with the carot or grape and then keep going
			// through the object. Right now it hits as if the gas is a solid object and bounces to the side
			// of the enemy sprite. 
			//Physics2D.IgnoreCollision(c.gameObject.collider2D, this.gameObject.rigidbody2D.collider2D);
		}		
	}
}
