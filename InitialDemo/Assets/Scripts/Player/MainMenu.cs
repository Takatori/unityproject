﻿using UnityEngine;
using System.Collections;
using System;

public class MainMenu : MonoBehaviour {

	public Color color1 = Color.red;
	public Color color2 = Color.blue;
	public float duration = 3.0F;
	void Update() {
		float t = Mathf.PingPong(Time.time, duration) / duration;
		camera.backgroundColor = Color.Lerp(color1, color2, t);
	}
	void ClearFlags() {
		camera.clearFlags = CameraClearFlags.SolidColor;
	}

	public void OnGUI () {
		float boxWidth = Screen.width*2/3;
		float boxHeight = Screen.height*2/3;
		float numButtons = 9;

		// Make a background box
		GUI.Box(new Rect((Screen.width/2)-(boxWidth/2),(Screen.height/2)-(boxHeight/2),boxWidth,boxHeight), "Main Menu");
		
		// Make the LoadLevel1 button. If it is pressed, Application.Loadlevel (5) will be executed
		if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(1*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Load Level 1")) {
			Application.LoadLevel(5);
		}
		
		// Make the LoadLevel2 button. If it is pressed, Application.Loadlevel (6) will be executed
		if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(2*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Load Level 2")) {
			Application.LoadLevel(6);
		}

		// Make the LoadLevel3 button. If it is pressed, Application.Loadlevel (7) will be executed
		if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(3*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Load Level 3")) {
			Application.LoadLevel(7);
		}

		// Make the SurvivalMode button. If it is pressed, Application.Loadlevel (8) will be executed
		if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(4*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Survival Mode")) {
			Application.LoadLevel(8);
		}
		
		// Make the Instructions button. If it is pressed, Application.Loadlevel (2) will be executed
		if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(5*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Instructions")) {
			Application.LoadLevel(2);
		}

		// Make the Objective button. If it is pressed, Application.Loadlevel (1) will be executed
		if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(6*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Objective")) {
			Application.LoadLevel(1);
		}

		// Make the PlayerReset button. If it is pressed, Player Preferences will be reset
		if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(7*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Reset Player")) {
			PlayerPrefs.SetInt("Exp", 0);
			PlayerPrefs.SetInt("Level", 1);
			PlayerPrefs.SetInt("GhostGoat", 0);
			Debug.Log("Player Preferences Reset to Zero");
		}

		// Make the Unlock Ghost Goat button. If it is pressed, Player Preferences will be reset
		if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(8*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Unlock Ghost Goat Cheat")) {
			PlayerPrefs.SetInt("GhostGoat", 1);
			Debug.Log("Ghost Goat Unlocked");
		}
		
		// Make the Quit button.
		if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(9*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Quit")) {
			Application.Quit();
			Destroy (this.gameObject);
		}

		if (PlayerPrefs.GetInt ("GhostGoat") == 1 && PlayerPrefs.GetInt ("GhostGoat") != 0) {
			GUI.Box(new Rect (Screen.width/2-170, Screen.height-40, 150, 20), "Ghost Goat Activated");
		}

		if (PlayerPrefs.GetInt ("Exp") == 0 && PlayerPrefs.GetInt ("Level") == 1 && PlayerPrefs.GetInt ("GhostGoat") == 0) {
			GUI.Box(new Rect (Screen.width/2+20, Screen.height-40, 200, 20), "Player Preferences Reset");
		}
	}
}