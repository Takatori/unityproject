﻿using UnityEngine;
using System.Collections;
using System;

public class TomatoSplat : MonoBehaviour {

	DateTime destroy = DateTime.MaxValue;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (destroy == DateTime.MaxValue)
		{
			destroy = DateTime.Now.AddSeconds(.15);
		}

		transform.localScale += new Vector3 (.4f, .4f, 0f);
		if (DateTime.Now > destroy)
		{
			Destroy(gameObject);
		}
	}

	// Event collision
	void OnCollisionEnter2D(Collision2D c)
	{
	    int damage = PlayerPrefs.GetInt("Level") * 2 + 3;
		// When hit (collision with an Enemy/Player),
		c.gameObject.SendMessage("takeDamage", damage);

	}

	// Set the position/direction of a vector
	void setPosition(Vector3 vec)
	{
		transform.position = vec;
	}
}
