﻿using UnityEngine;
using System.Collections;
using System;

public class HealthBar : MonoBehaviour {

	private int maxHealth = 100;
	public int curHealth = 100;
	private float healthBarlength = Screen.width / 3;

	private int maxExp = 5;
	public int curExp = 100;
	private float expBarlength = Screen.width / 3;

	private DateTime hitTimer = DateTime.MinValue;
	private Boolean tabletBroken = false;

	// Use this for initialization
	void Start () {
		hitTimer = DateTime.MinValue;
		gameObject.renderer.enabled = true;
		Physics2D.IgnoreLayerCollision(9,8, false);
		Physics2D.IgnoreLayerCollision(9,11, false);
	}
	
	// Update is called once per frame
	void Update () {
		// Adjust health & healthBar
		AdjustcurHealth (0) ;

		// Adjust experience & experienceBar
		//expBarlength = (Screen.width/3)*(curExp/(float)maxExp);

		if(hitTimer != DateTime.MinValue)
		{
			if (DateTime.Now.Millisecond % 500 > 250)
			{
				gameObject.renderer.enabled = false;
			}
			else
			{
				gameObject.renderer.enabled = true;
			}
			if (hitTimer < DateTime.Now)
			{
				hitTimer = DateTime.MinValue;
				gameObject.renderer.enabled = true;
				Physics2D.IgnoreLayerCollision(9,8, false);
				Physics2D.IgnoreLayerCollision(9,11, false);
			}
		}
	}

	void OnGUI () {
		GUI.Label(new Rect(150, 10, 100, 20), "Health:");
		GUI.Box(new Rect(150, 28, healthBarlength, 20), curHealth + "/" + maxHealth);

		/*
		GUI.Label(new Rect(150, 50, 500, 20), "Experience to Next Level:"
		          + (((PlayerPrefs.GetInt("Level")*PlayerPrefs.GetInt("Level"))+(5*PlayerPrefs.GetInt("Level"))) 
		          + ((-1)*PlayerPrefs.GetInt("Exp"))));
		*/

		/*
		if (maxExp >= 5) {
			GUI.Box (new Rect (150, 68, expBarlength, 20), PlayerPrefs.GetInt ("Exp") + "/" + maxExp+1);
		}
		else {
			GUI.Box (new Rect (150, 68, 5, 20), PlayerPrefs.GetInt ("Exp") + "/" + maxExp+1);
		}
		*/

		//GUI.Label (new Rect (150, 64, 100, 20), "Player Level: " + PlayerPrefs.GetInt ("Level"));

		if (tabletBroken) {
			Time.timeScale = 0;

			float boxWidth = Screen.width*55/100;
			float boxHeight = Screen.height*55/100;
			float numButtons = 7;
			
			// Make a background box
			GUI.Box(new Rect((Screen.width/2)-(boxWidth/2),(Screen.height/2)-(boxHeight/2),boxWidth,boxHeight), "You lost the level. Sorry.");
			
			// Make the LoadLevel1 button. If it is pressed, Application.Loadlevel (5) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(1*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Load Level 1")) {
				Application.LoadLevel(5);
			}
			
			// Make the LoadLevel2 button. If it is pressed, Application.Loadlevel (6) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(2*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Load Level 2")) {
				Application.LoadLevel(6);
			}
			
			// Make the LoadLevel3 button. If it is pressed, Application.Loadlevel (7) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(3*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Load Level 3")) {
				Application.LoadLevel(7);
			}

			// Make the SurvivalMode button. If it is pressed, Application.Loadlevel (8) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(4*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Survival Mode")) {
				Application.LoadLevel(8);
			}
			
			// Make the Instructions button. If it is pressed, Application.Loadlevel (2) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(5*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Instructions")) {
				Application.LoadLevel(2);
			}
			
			// Make the Objective button. If it is pressed, Application.Loadlevel (1) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(6*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Objective")) {
				Application.LoadLevel(1);
			}
			
			// Make the Quit button.
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(7*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Quit")) {
				Application.Quit();
				Destroy (this.gameObject);
			}

			/*
			float boxWidth = Screen.width / 2;
			float boxHeight = Screen.height / 2;
			float numButtons = 4;
			
			// Make a background box. Taken from MainMenu.cs
			GUI.Box (new Rect ((Screen.width / 2) - (boxWidth / 2), (Screen.height / 2) - (boxHeight / 2), boxWidth, boxHeight), "You lost the level. Sorry.");
			
			// Make the Main Menu button. If it is pressed, Application.Loadlevel (0) will be executed
			if(GUI.Button(new Rect((Screen.width/2)-(boxWidth/2)+10,(Screen.height/2)-(boxHeight/2)+(1*boxHeight/(numButtons+1))-10,boxWidth-20,boxHeight/(numButtons+1)), "Main Menu")) {
				Application.LoadLevel(0);
			}
			
			// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (2 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Load Level 1")) {
				Application.LoadLevel (1);
			}
			
			// Make the second button. If it is pressed, Application.Loadlevel (2) will be executed
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (3 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Load Level 2")) {
				Application.LoadLevel (2);
			}
			
			// Make the third button. If it is pressed, Application.Loadlevel (3) will be executed
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (4 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Load Level 3")) {
				Application.LoadLevel (3);
			}
			
			/*
			// Make the quit button.
			if (GUI.Button (new Rect ((Screen.width / 2) - (boxWidth / 2) + 10, (Screen.height / 2) - (boxHeight / 2) + (5 * boxHeight / (numButtons + 1)) - 10, boxWidth - 20, boxHeight / (numButtons + 1)), "Quit Level")) {
				Application.Quit ();
				Application.LoadLevel(0);
			}
			*/
		}
	}

	public void AdjustcurHealth (int adj) {
		curHealth += adj;
		if(curHealth < 0)
			curHealth = 0;
		if(curHealth > maxHealth)
			curHealth = maxHealth;
		if(maxHealth < 1)
			maxHealth = 1;
		if (curHealth == 0) {
			GUI.Box(new Rect(Screen.width/2, Screen.height/2, 100, 100), "You're Dead :(");
		}
		healthBarlength = (Screen.width/3)*(curHealth/(float)maxHealth);
	}
	
	private ArrayList ids = new ArrayList();
	
	public void takeDamage(int dmg)
	{
		int[] temp = {dmg, int.MinValue};
		takeDamage (temp);
	}
	
	void takeDamage(int[] arg)
	{
		Debug.Log ("take damage");
		if(arg[1] == int.MinValue || !ids.Contains(arg[1]))
		{
			curHealth -= arg[0];
			hitTimer = DateTime.Now.AddSeconds(1);
			// Weird workaround since the ignorelayercollision doesn't seem to work
			// when there are objects from the 2 layers already colliding.
			gameObject.collider2D.enabled = false;
			Physics2D.IgnoreLayerCollision(9,8);
			Physics2D.IgnoreLayerCollision(9,11);
			gameObject.collider2D.enabled = true;
			if (curHealth <= 0) 
			{
				tabletBroken = true;
				//Application.LoadLevel(0);
				//Destroy (this.gameObject);
			}
			if (arg[1] != int.MinValue)
			{
				ids.Add(arg[1]);
			}
		}
	}
}
