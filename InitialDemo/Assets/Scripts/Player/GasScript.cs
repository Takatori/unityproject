﻿using UnityEngine;
using System.Collections;
using System;

public class GasScript : MonoBehaviour {

	
	// Tag that this projectile will hit and damage
	public String tagToHit = "Enemy";
	// Speed of projectile
	public int projectileSpeed = 3;
	// Direction of projectile
	public Vector3 dir;
	// Whether to draw projectiles
	public Boolean spawned = false;
	private int level = 1;
	private Vector3 player;
	public DateTime destroy;
	public int id = 0;

	
	void Start()
	{
		// Initialize projectile tag
		gameObject.tag = "Projectile";

		try
		{
			level = PlayerPrefs.GetInt("Level");
			Debug.Log("Level = " + level);
		}
		catch(Exception e)
		{
			Debug.Log("caught exception ");
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		try
		{
			level = PlayerPrefs.GetInt("Level");
			Debug.Log("Level = " + level);
		}
		catch(Exception e)
		{
			Debug.Log("caught exception ");
		}
		Vector3 temp = GameObject.FindGameObjectsWithTag ("Player")[0].rigidbody2D.transform.position;
		this.transform.position += temp - player;
		player = temp;
		if(spawned == true) 
		{
			this.transform.position += (Vector3)dir * projectileSpeed * Time.deltaTime;
		}
		// Destroy object
		if(DateTime.Now > destroy)
		{
			Destroy (this.gameObject);
		}
	}
	
	// Event collision
	void OnCollisionEnter2D(Collision2D c)
	{
		// When hit (collision with an Enemy/Player),
		// send message to decrement the Enemy/Player's health
		if (String.Equals(c.gameObject.tag, tagToHit, StringComparison.OrdinalIgnoreCase))
		{
			int dmg = 2 * level + 3;
			int[] temp = {dmg, id};
			c.gameObject.SendMessage("takeDamage", temp);
			// This is only implemented in the beta version of unity
			// This would make it so that our gas would collide with the carot or grape and then keep going
			// through the object. Right now it hits as if the gas is a solid object and bounces to the side
			// of the enemy sprite. 
			//Physics2D.IgnoreCollision(c.gameObject.collider2D, this.gameObject.rigidbody2D.collider2D);
		}		
	}

	void setId(int i)
	{
		id = i;
	}

	// Create a vector
	void setVector(Vector3 vec)
	{
		dir = vec;
	}
	
	// Set the position/direction of a vector
	void setPosition(Vector3 vec)
	{
		int level = 1;
		level = PlayerPrefs.GetInt ("Level");
		transform.position = vec;
		spawned = true;
		destroy = DateTime.Now.AddSeconds(.25 + (level*.05));
		player = GameObject.FindGameObjectsWithTag ("Player")[0].rigidbody2D.transform.position;
	}
}