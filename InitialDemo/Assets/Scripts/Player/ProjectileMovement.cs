using UnityEngine;
using System;

public class ProjectileMovement : MonoBehaviour {

	// Tag that this projectile will hit and damage
	public String tagToHit = "Enemy";
	// Speed of projectile
	public int projectileSpeed = 3;
	// Direction of projectile
	public Vector3 dir;
	// destination for the splat
	public Vector3 dest;
	// Whether to draw projectiles
	public Boolean spawned = false;

	
	void Start()
	{
		// Initialize projectile tag
		gameObject.tag = "Projectile";
	}

	// Update is called once per frame
	void Update () {
		if(spawned == true) 
		{
			this.transform.position += (Vector3)dir * projectileSpeed * Time.deltaTime;
		}
		if (tagToHit == "Enemy")
		{
			Debug.Log("dest" + dest + "object" + transform.position);
			if((dest - gameObject.transform.position).magnitude < .2f)
			{
				GameObject tomatosplat = (GameObject)Instantiate(Resources.Load("TomatoSplat"));
				tomatosplat.SendMessage("setPosition", transform.position);
				Destroy (this.gameObject);
			}
		}
	}

	// Event collision
	void OnCollisionEnter2D(Collision2D c)
	{
		int damage = 5;
		if(tagToHit == "Enemy")
		{
			damage = PlayerPrefs.GetInt("Level") * 2 + 3;
		}
		// When hit (collision with an Enemy/Player),
		// send message to decrement the Enemy/Player's health
		if (String.Equals(c.gameObject.tag, tagToHit, StringComparison.OrdinalIgnoreCase))
		{
			c.gameObject.SendMessage("takeDamage", damage);
		}
		if(tagToHit == "Enemy")
		{
			GameObject tomatosplat = (GameObject)Instantiate(Resources.Load("TomatoSplat"));
			tomatosplat.SendMessage("setPosition", transform.position);
		}
		// Destroy projectile
		Destroy (this.gameObject);
	}

	// Create a vector
	void setVector(Vector3 vec)
	{
		dir = vec;
		spawned = true;
	}

	void setDestination(Vector3 vec)
	{
		dest = vec;
	}

	// Set the position/direction of a vector
	void setPosition(Vector3 vec)
	{
		transform.position = vec;
	}
}
